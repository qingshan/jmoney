# 金钱管理
------------------------

>#### 本网站后台基于Jfinal开发，数据库为Postgresql或Mysql。
>#### 演示地址：[http://jmoney.sturgeon.mopaas.com](http://jmoney.sturgeon.mopaas.com)
>#### 测试账号: admin/admin123 或 test/123456

# 平台部署说明
 1. 下载jmoney项目：配置数据库文件/jmoney/src/conf/db.properties
 2. 安装mysql数据库，创建用户和数据库。 
 3. 运行mysql.sql和create_data.sql文件  
 4. 如果是postgre、sqlite3，只需要运行对应sql文件即可  

# 鸣谢
 1. [JFinal](http://www.oschina.net/p/jfinal)
 2. [beetl](http://ibeetl.com/community/)
 3. [oschina](http://www.oschina.net/)

# 更新说明

>## jmoney4.1
> 1. 全面修改模板后缀。

>## jmoney4.0
> 1. 由于许多人下载代码后，会反映CRUD封装可读性太差，这次对前台进行了全面重构。
> 2. 把CRUD极度封装改为代码自动生成，这样对简单的增删改查仍然通过配置，
>    而不写任何前台代码，并且项目的可读性会好很多，利于新人学习。
> 3. 对于个人学习，现在的增删改查一目了然。

>## jmoney3.3
> 1. 部分小功能修改，对模板功能进行备份。

>## jmoney3.2
> 1. 已全面支持模块功能，可以通过类和XML两种方式进行配置

>## 2014-10-11 ##
> 1. 已支持模板的XML配置。
> 2. 实现通过类注入生成XML配置文件功能。
> 3. 实现XML解析CRUD功能。

>## 2014-10-09 ##
> 1. 对模板功能进行全面优化。
> 2. 现模板已支持checkbox，radio，select，数据字典，data等格式.
> 3. 对模板信息进行了抽取，可配置处理。

>## 2014-09-22 ##
> 1. 数据库文件优化

>## jmoney3.1
> 1. 初步完成了自定义模板增删改代码

>## 2014-09-15 ##
> 1. 模板功能优化，已初步完成封装。

>## jmoney3.0
> 1. 模板引擎已全面升级为Beetl
> 2. 加入了个人模板功能，可以方便的时间简单页面的增删改功能

>## 2014-08-28~08-30
> 1. 将前台模板升级为Beetl

>## 2014-08-27 ##
> 1. 加入统一模板，仍待完善 ####
> 2. 将项目管理，联系人管理和用户管理页面修改为模板；删除原有页面。
> 3. 模板加入inputType以及HTML5验证支持

>## 2014-08-26
> 1. 加入联系人管理

>## 2014-08-25
> 1. 更新后台导航栏。修改为bootstrap导航，更符合页面风格
> 2. 修改系统管理跳转友好型。如果已登录，直接进入，不再需要输入账号，密码。
> 3. 修复sqlite3中user查询和money查询问题
> 4. 首页加入特别鸣谢

>## money2.2
> 1. 这是前台模板支持JSP的最后一个版本

>## money2.0
> 1. jfinal更新到最新版本1.8
> 2. 修改了基本封装，源码见jflyfox_base和jflyfox_jfinal

# 开源赞助
![OSC@GIT](http://static.oschina.net/uploads/space/2015/0213/104940_ZKNb_166354.png "开源赞助我(支付宝)")

