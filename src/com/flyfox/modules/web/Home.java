package com.flyfox.modules.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.flyfox.jfinal.base.BaseController;
import com.flyfox.jfinal.component.annotation.ControllerBind;
import com.flyfox.modules.money.TbMoney;
import com.flyfox.modules.project.TbProject;
import com.flyfox.util.DateUtils;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.DbKit;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.dialect.Dialect;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.activerecord.dialect.Sqlite3Dialect;

import conf.auto.BeeltFunctions;

@ControllerBind(controllerKey = "/web")
public class Home extends BaseController {

	private static final int user_mobile_id = -1;
	private static final String path = "/pages/web/";

	public void index() {
		setTotalMoney();

		String sql = "";
		// 项目总金额
		// 名称
		String formatSql = "select sum(%1$s) as a,project_id from tb_money group by project_id order by project_id";
		JSONObject projectMoney = new JSONObject();
		sql = "select id,name from tb_project where id in (select project_id from tb_money)  order by id";
		JSONArray array = new JSONArray();
		List<Record> listRecord = Db.find(sql);
		for (Record record : listRecord) {
			array.add(record.getStr("name"));
		}
		projectMoney.put("name", array);
		// 预算
		sql = String.format(formatSql, "case when type = '24' then amount else 0 end");
		projectMoney.put("ys", getArray(sql, "a"));
		// 支出
		sql = String.format(formatSql, "case when type = '23' then amount else 0 end");
		projectMoney.put("zc", getArray(sql, "a"));
		// 收入
		sql = String.format(formatSql, "case when type = '22' then amount else 0 end");
		projectMoney.put("sr", getArray(sql, "a"));
		setAttr("projectMoney", projectMoney.toJSONString());

		// 项目信息
		sql = "select * from tb_project";
		List<TbProject> listProject = TbProject.dao.find(sql);
		setAttr("listProject", listProject);

		// 费用明细
		sql = "select t.*,p.name as project_name from tb_money t,tb_project p where t.project_id = p.id order by pay_time desc";
		List<TbMoney> listMoney = TbMoney.dao.find(sql);
		setAttr("listMoney", listMoney);

		// 项目费用明细
		Map<Integer, List<TbMoney>> mapMoney = new HashMap<Integer, List<TbMoney>>();
		for (TbMoney money : listMoney) {
			List<TbMoney> listTmp = mapMoney.get(money.getInt("project_id"));
			if (listTmp == null) {
				listTmp = new ArrayList<TbMoney>();
			}
			listTmp.add(money);
			mapMoney.put(money.getInt("project_id"), listTmp);
		}
		setAttr("mapMoney", mapMoney);

		// 项目列表
		String optionProjectString = BeeltFunctions.sel(getJsonData("select id,name from tb_project", "id", "name"),
				null);
		setAttr("optionList", optionProjectString);

		renderAuto(path + "home.html");

	}

	protected void setTotalMoney() {
		JSONArray totalMoney = new JSONArray();
		// mysql 总费用报表
		String sql = "select COALESCE(sum(case when type = '24' then amount else 0 end),0) a,"
				+ "COALESCE(sum(case when type = '23' then amount else 0 end),0) b,"
				+ "COALESCE(sum(case when type = '22' then amount else 0 end),0) c from tb_money";
		Record totalMoneyRecord = Db.findFirst(sql);
		Dialect dialect = DbKit.getConfig().getDialect();
		if (dialect instanceof Sqlite3Dialect) {
			// 预算
			totalMoney.add(totalMoneyRecord.getInt("a"));
			// 支出
			totalMoney.add(totalMoneyRecord.getInt("b"));
			// 收入
			totalMoney.add(totalMoneyRecord.getInt("c"));
		} else if (dialect instanceof MysqlDialect) {
			// 预算
			totalMoney.add(totalMoneyRecord.getBigDecimal("a"));
			// 支出
			totalMoney.add(totalMoneyRecord.getBigDecimal("b"));
			// 收入
			totalMoney.add(totalMoneyRecord.getBigDecimal("c"));
		}

		setAttr("totalMoney", totalMoney.toJSONString());
	}

	private JSONArray getArray(String sql, String name) {
		JSONArray array = new JSONArray();
		List<Record> list = Db.find(sql);
		for (Record record : list) {
			Dialect dialect = DbKit.getConfig().getDialect();
			if (dialect instanceof Sqlite3Dialect) {
				array.add(record.getInt(name));
			} else if (dialect instanceof MysqlDialect) {
				array.add(record.getBigDecimal(name));
			}

		}
		return array;
	}

	/**
	 * 保存项目
	 * 
	 * 2014年8月13日 上午12:14:44 flyfox 330627517@qq.com
	 */
	public void save_project() {
		Integer pid = getParaToInt();
		TbProject model = getModel(TbProject.class);
		if (pid != null && pid > 0) { // 更新
			model.update();
		} else { // 新增
			model.remove("id");
			model.put("type", "1");
			model.put("create_id", user_mobile_id);
			model.put("create_time", DateUtils.getNow());
			model.save();
		}
		redirect("/web#project");
	}

	/**
	 * 保存费用
	 * 
	 * 2014年8月13日 上午12:14:53 flyfox 330627517@qq.com
	 */
	public void save_money() {
		Integer pid = getParaToInt();
		TbMoney model = getModel(TbMoney.class);
		if (pid != null && pid > 0) { // 更新
			model.update();
		} else { // 新增
			model.remove("id");
			model.put("create_id", user_mobile_id);
			model.put("create_time", DateUtils.getNow());
			model.save();
		}
		redirect("/web#project");

	}
}
