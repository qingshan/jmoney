package com.flyfox.modules.money;

import com.flyfox.jfinal.base.BaseModel;
import com.flyfox.jfinal.component.annotation.ModelBind;

@ModelBind(table="tb_money")
public class TbMoney extends BaseModel<TbMoney> {

	private static final long serialVersionUID = 1L;
	public static final TbMoney dao = new TbMoney();

}
