package com.flyfox.modules.project;

import com.flyfox.jfinal.base.BaseModel;
import com.flyfox.jfinal.component.annotation.ModelBind;

@ModelBind(table = "tb_project")
public class TbProject extends BaseModel<TbProject> {

	private static final long serialVersionUID = 1L;
	public static final TbProject dao = new TbProject();
	
}
